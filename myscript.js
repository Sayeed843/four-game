var playerOne = prompt("Player One: Enter your Name, you will get blue color");
var playerOneColor = "rgb(30, 144, 255)";

var playerTwo = prompt("Player Two: Enter you name, you will get green color");
var playerTwoColor = "rgb(34, 139, 34)";



var table = $("table tr");


function getColor(row, col){
    return table.eq(row).find("td").eq(col).find("button").css("background-color");
}


function setColor(row, col, color){
    return table.eq(row).find("td").eq(col).find("button").css("background-color", color);
}

function getRow(col){
    var checkColor ="";
    for(var row=4; row>=0; row--){
        checkColor = getColor(row, col);

        if (checkColor === "rgb(211, 211, 211)"){
            return row;
        }
    }
}


function WinnerChecker(one, two, three, four){
    return one === two && one === three && one === four &&
    one !== "rgb(211, 211, 211)" && one !== undefined;
}



function horizontalWin(){
    for(var row=4; row >=0; row--){
        for(var col=0; col<4;col++){

            if(WinnerChecker(getColor(row,col), getColor(row, col+1),
            getColor(row, col+2), getColor(row, col+3))){
                return true;
            }else{
                continue;
            }
        }
    }
}


function verticalWin(){
    for(var col=6; col>=0; col--){
        for(var row=4; row>2;row--){

            if(WinnerChecker(getColor(row,col), getColor(row-1,col),
            getColor(row-2,col), getColor(row-3,col))){
                return true;
            }else{
                continue;
            }
        }
    }
}


function cornerWin(){
    for(var col=0; col<4;col++){
        for(var row=4;row>2;row--){

            if(WinnerChecker(getColor(row,col),getColor(row-1,col+1),
            getColor(row-2,col+2),getColor(row-3,col+3))){
                return true;

            }else if(WinnerChecker(getColor(row,col),getColor(row-1,col-1),
            getColor(row-2,col-2),getColor(row-3,col-3))){
                return true;

            }else{
                continue;
            }
        }
    }
}

var currentPlayer = 1;
var currentPlayerName = playerOne;
var nextPlayerName = playerTwo;
var currentColor = playerOneColor;

$("h3").text(playerOne +" please, pick your choosen column");

$(".board button").on("click", function(){
    var col = $(this).closest("td").index();
    var row = getRow(col);
    // console.log(row);

    if(currentPlayer === 1){
        currentPlayerName = playerOne;
        currentColor = playerOneColor;
        console.log("Current Player: "+currentPlayer);
        setColor(row, col, currentColor);
        nextPlayerName = playerTwo;
    }else{
        currentPlayerName = playerTwo;
        currentColor = playerTwoColor;
        console.log("Current Player: "+currentPlayer);
        setColor(row, col, currentColor);
        nextPlayerName = playerOne;
    }

    console.log("Corner Win: "+cornerWin());
    if(horizontalWin() || verticalWin() || cornerWin()){
        alert("Player: "+currentPlayerName+" you are win");
        table.find("td").find("button").attr("disabled", true);
    }

    currentPlayer = currentPlayer * -1;
    $("h3").text(nextPlayerName +" please, pick your choosen column");
})
